﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Threading;
using System.Threading;
using Un4seen.Bass;
using Un4seen.BassWasapi;
using System.IO.Ports;

namespace Color_organ
{
    class Analyzer
    {
        private const int DELAY = 512;

        private ComboBox comboDevice;
        private ComboBox comboPort;

        private Thread thread;
        private static SerialPort sp;

        private static bool enabled;
        private static int selectedDevice;

        private float[] fft;
        private WASAPIPROC process;


        public Analyzer(ComboBox comboDevice, ComboBox comboPort)
        {
            this.comboDevice = comboDevice;
            this.comboPort = comboPort;

            sp = new SerialPort();

            fft = new float[1024];
            process = new WASAPIPROC(Process);
            
            init();
        }


        private void init()
        {
            BassNet.Registration("jan@jstanek.cz", "2X1931414192839");

            comboDevice.Items.Clear();
            int count = BassWasapi.BASS_WASAPI_GetDeviceCount();
            for (int i = 0; i < count; i++)
            {
                var device = BassWasapi.BASS_WASAPI_GetDeviceInfo(i);
                if (device.IsEnabled && device.IsLoopback)
                    comboDevice.Items.Add(i + "-" + device.name);
            }

            Bass.BASS_SetConfig(BASSConfig.BASS_CONFIG_UPDATETHREADS, false);
            Bass.BASS_Init(0, 44100, BASSInit.BASS_DEVICE_DEFAULT, IntPtr.Zero);
        }


        public void start()
        {
            enabled = true;

            selectedDevice = Int32.Parse(comboDevice.SelectedValue.ToString().Split('-')[0]);

            BassWasapi.BASS_WASAPI_Init(selectedDevice, 0, 0, BASSWASAPIInit.BASS_WASAPI_BUFFER, 1f, 0.05f, process, IntPtr.Zero);
            BassWasapi.BASS_WASAPI_Start();

            sp.PortName = comboPort.SelectedItem.ToString();
            sp.BaudRate = 115200;
            if (!sp.IsOpen)
                sp.Open();

            thread = new Thread(background);
            thread.Start();
        }

        public void stop()
        {
            enabled = false;
            thread.Join();
            sp.Close();
            BassWasapi.BASS_WASAPI_Free();
        }

        private void background()
        {


            while (enabled)
            {
                double[] res = new double[3];
                for (int i = 0; i < 3; i++)
                {
                    res[i] = 0;
                }

                for (int i = 0; i < 16; i++)
                {
                    BassWasapi.BASS_WASAPI_GetData(fft, (int)(BASSData.BASS_DATA_FFT2048 | BASSData.BASS_DATA_FFT_REMOVEDC));

                    float[] avgs = new float[3];

                    computeBands(3, fft, avgs);

                    for (int j = 0; j < 3; j++)
                    {
                        res[j] += avgs[j];
                    }

                    Thread.Sleep(512 / 16);
                }

                for (int i = 0; i < 3; i++)
                {
                    res[i] *= 1000;
                    res[i] = Math.Sqrt(res[i]);  
                    //res[i] /= 250;
                    if (res[i] > 255)
                        res[i]= 255;
                    if (res[i] < 1)
                        res[i] = 1;
                }

                double max = 0;
                int maxi = 0;
                for (int i = 0; i < 1024; i++)
                {
                    if (fft[i] > max)
                    {
                        maxi = i;
                        max = fft[i];
                    }
                }

                
                byte[] buffer = new byte[3];

                buffer[0] = (byte)res[0];
                buffer[1] = (byte)res[1];
                buffer[2] = (byte)res[2];

                sp.Write(buffer, 0, 3);

                System.Console.WriteLine(maxi + "-" + (int)res[0] + " " + (int)res[1] + " " + (int)res[2]);


                /*
                int from = 0, to;
                
                float max = 0;
                int maxi = 0;

                for (int i = 0; i < 1023; i++)
                {
                    if (fft[i] > max) { 
                        maxi = i;
                        max = fft[i];
                    }
                }

                double b = 0;
                for (int i = 0; i < 7; i++)
                    b += fft[i];
                b *= 5000;
                b /= 6;
                
                double g = 0;
                for (int i = 7; i < 46; i++)
                    g += fft[i];
                g *= 5000;
                g /= 38;

                double r = 0;
                for (int i = 46; i < 358; i++)
                    r += fft[i];
                r *= 5000;
                r /= 311;

                
                
                byte[] buffer = new byte[3];

                buffer[0] = (byte)b;
                buffer[1] = (byte)g;
                buffer[2] = (byte)r;

                sp.Write(buffer, 0, 3);
                
                System.Console.WriteLine(maxi + "-" + (int)b + " " + (int)g + " " + (int)r);
                */
                /*
                for (int i = 0; i < 3; i++)
                {
                    float peak = 0;

                    to = (int)Math.Pow(2, i * 10.0 / 2);
                    if (to > 1023)
                        to = 1023;
                    if (to <= from)
                        to = from + 1;

                    for (; from < to; from++)
                    {
                        if (peak < fft[1 + from])
                            peak = fft[1 + from];
                    }
                    int y = (int)(Math.Sqrt(peak) * 3 * 255 - 4);
                    if (y > 255)
                        y = 255;
                    if (y < 0)
                        y = 0;
                    System.Console.Write("{0, 3} ", y);
                }
                System.Console.WriteLine();
                */
            }
        }

        private void computeBands(int bands, float[] fft, float[] avgs)
        {
            double b = 0;
            //for (int i = 1; i < 13; i++)
            for (int i = 0; i < 7; i++)
                b += fft[i];
            avgs[0] = (float)(b);

            double g = 0;
            //for (int i = 13; i < 98; i++)
            for (int i = 7; i < 46; i++)
                g += fft[i];
            avgs[1] = (float)(g);

            double r = 0;
            //for (int i = 98; i < 779; i++)
            for (int i = 64; i < 358; i++)
                r += fft[i];
            avgs[2] = (float)(r);



            /*
            for (int x = 0; x < bands; x++)
            {
                int b0 = 0, y;

                float peak = 0;
                int b1 = (int)Math.Pow(2, x * 10.0 / (bands - 1));
                if (b1 > 1023) b1 = 1023;
                if (b1 <= b0) b1 = b0 + 1;
                int c = b1 - b0;
                for (; b0 < b1; b0++)
                {
                    peak += fft[1 + b0];
                }
                y = (int)Math.Sqrt(100000*peak/c);
            
                avgs[x] = y;
            }
            */

            /*

            for (int i = 0; i < bands; i++)
            {
                float avg = 0;
                int lowFreq;
                if (i == 0)
                    lowFreq = 0;
                else
                    lowFreq = (int)((44100 / 2) / (float)Math.Pow(2, bands - i));
                int hiFreq = (int)((44100 / 2) / (float)Math.Pow(2, bands - 1 - i));
                int lowBound = freqToIndex(lowFreq);
                int hiBound = freqToIndex(hiFreq);
                for (int j = lowBound; j <= hiBound; j++)
                {
                    avg += fft[j];
                }
                avg /= (hiBound - lowBound + 1);
                avgs[i] = avg;
            }
            */
        }

        private float getBandWidth()
        {
            return (float)44100 / (float)2048;
        }

        private int freqToIndex(int freq)
        {
            if (freq < getBandWidth() / 2) return 0;
            if (freq > 44100 / 2 - getBandWidth() / 2) return 512;
            float fraction = (float)freq / (float)44100;
            int i = (int)(2048 * fraction);
            return i;
        }

        private int Process(IntPtr buffer, int length, IntPtr user)
        {
            return length;
        }
    }
}
