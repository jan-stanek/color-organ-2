﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Un4seen.Bass;
using Un4seen.BassWasapi;
using System.IO.Ports;



namespace Color_organ
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private Analyzer analyzer;

        public MainWindow()
        {
            InitializeComponent();
            analyzer = new Analyzer(comboDevice, comboPort);
           
        }


        private void btnStart_Click(object sender, RoutedEventArgs e)
        {
            comboPort.IsEnabled = false;
            comboDevice.IsEnabled = false;
            btnStart.IsEnabled = false;
            btnStop.IsEnabled = true;
            analyzer.start();
        }

        private void btnStop_Click(object sender, RoutedEventArgs e)
        {
            comboPort.IsEnabled = true;
            comboDevice.IsEnabled = true;
            btnStart.IsEnabled = true;
            btnStop.IsEnabled = false;
            analyzer.stop();
        }

        private void comboPort_DropDownOpened(object sender, EventArgs e)
        {
            comboPort.Items.Clear();
            var ports = SerialPort.GetPortNames();
            foreach (var port in ports)
                comboPort.Items.Add(port);
        }
    }
}
